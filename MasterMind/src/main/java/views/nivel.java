package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.EmptyBorder;

public class nivel extends JFrame {

	private JPanel contentPane;
	private JRadioButton facil,intermedio,dificil;
	private ButtonGroup grupo1;
	/**
	 * Create the frame.
	 */
	public nivel() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		facil = new JRadioButton("Fácil",true);
		facil.setBounds(146, 70, 127, 25);
		contentPane.add(facil);
		
		intermedio = new JRadioButton("Intermedio",false);
		intermedio.setBounds(146, 100, 127, 25);
		contentPane.add(intermedio);
		
		dificil = new JRadioButton("Dificil",false);
		dificil.setBounds(146, 130, 127, 25);
		contentPane.add(dificil);
		
		grupo1 = new ButtonGroup();
		grupo1.add(facil);
		grupo1.add(intermedio);
		grupo1.add(dificil);
		
		JButton aceptar = new JButton("Aceptar");
		aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		aceptar.setBounds(75, 185, 97, 25);
		aceptar.addActionListener(accept);
		contentPane.add(aceptar);
		
		JButton cancelar = new JButton("Cancelar");
		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		cancelar.setBounds(207, 185, 97, 25);
		cancelar.addActionListener(cancel);
		contentPane.add(cancelar);
		
		JLabel lblNewLabel = new JLabel("Seleccione la dificultad deseada:");
		lblNewLabel.setBounds(91, 45, 243, 16);
		contentPane.add(lblNewLabel);
	}
	
	ActionListener accept = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			principal p;
			switch (devuelveDificultad()) {
			case 4:
				  p= new principal(4);
			      p.setVisible(true);
				break;
			case 5:
				  p= new principal(5);
			      p.setVisible(true);
				break;
			case 6:
				 p= new principal(6);
			     p.setVisible(true);
				break;
			}
			dispose();
		}
	};
	
	ActionListener cancel = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
	};
	
	
	
	
	private int devuelveDificultad() {
		int dif=0;
		if(facil.isSelected())
			dif=4;
		if(intermedio.isSelected())
			dif=5;
		if(dificil.isSelected())
			dif=6;
		return dif;
	}
}
