package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dto.combinacion;
import dto.logica;

import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ScrollPaneConstants;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JToolBar;
import java.awt.SystemColor;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;

public class principal extends JFrame {

	private static final int SEPH = 15;
	private static final int SEPV = 25;
	private static final int XINT = 20;
	private static final int YINT = 25;
	private static final int WIDTHBUTTON = 85;
	private static final int HEIGTHBUTTON = 15;
	private JPanel contentPane;
	private logica l;
	private JButton jugar;
	private boolean ganado=false,perdido=false;
	

	/**
	 * Create the frame.
	 */
	public principal(int nivel) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 325);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		l = new logica(nivel);
		l.anyadeColores(generaConjunto(nivel, 300, 60,false,true));
		l.anyadeSecreto(generaConjunto(l.getNivel(), 300, 180,false,true));
		l.anyadeIntento(generaConjunto(l.getNivel(),XINT,l.getTurno()*SEPV+YINT,true,false));
		l.anyadeAPantalla(contentPane, 0, nivel, 2);
		l.anyadeAPantalla(contentPane, 0, nivel, 3);
		l.anyadeAPantalla(contentPane, l.getTurno(), l.getNivel(),0);
		
		jugar = new JButton("PRUEBA");
		jugar.setHorizontalAlignment(SwingConstants.LEFT);
		jugar.setBounds(l.getPosBut(XINT),YINT,WIDTHBUTTON,HEIGTHBUTTON);
		contentPane.add(jugar);
		
		JLabel lblNewLabel = new JLabel("Colores");
		lblNewLabel.setBounds(295, 24, 85, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Combinacion secreta");
		lblNewLabel_1.setBounds(276, 144, 144, 16);
		contentPane.add(lblNewLabel_1);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBackground(SystemColor.activeCaption);
		toolBar.setBounds(0, 0, 432, 18);
		contentPane.add(toolBar);
		
		JMenuBar menuBar = new JMenuBar();
		toolBar.add(menuBar);
		
		JMenu mnNewMenu = new JMenu("Archivo");
		mnNewMenu.setBackground(SystemColor.activeCaption);
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Salir");
		mnNewMenu.add(mntmNewMenuItem);
		mntmNewMenuItem.addActionListener(close);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Nuevo juego");
		mnNewMenu.add(mntmNewMenuItem_1);
		mntmNewMenuItem_1.addActionListener(nuevoJuego);
		jugar.addActionListener(prueba);
	}
	
	/**
	 * Genera una combinacion de "cantidad" numero de botones 
	 * teniendo el primer boton la posicion x e y pasadas
	 * por parametro tamaño 15x15 y "SEP" de separacion
	 * @param cantidad: numero de botones a generar
	 * @param x: posicion horizontal del primer boton
	 * @param y: posicion vertical del primer boton
	 * @return una objeto de tipo combinacion con comb inicializado
	 */
	private combinacion generaConjunto(int cantidad,int x,int y,boolean conListener,boolean isColores) {
		combinacion conjunto = new combinacion(cantidad, x, y);
		for (int i = 0; i < cantidad; i++) {
			if(isColores)
				conjunto.addButton(new JButton(""),i*SEPH+x,y, 15, 15,Color.WHITE,false);
			else
				conjunto.addButton(new JButton(""),i*SEPH+x,y, 15, 15,l.getDefaultColor(),true);
			if(conListener)
				conjunto.getComb().get(i).addActionListener(cambioColor);
		}	
		return conjunto;
	}
	
	/**
	 * Genera el resultado del turno actual
	 * @return devulve un objeto combinacion con 
	 * el resultado del intento del turno actual
	 */
	private combinacion generaResultado(int x, int y) {
		ArrayList<Integer> secuencia = l.comparaColores();
		combinacion c = new combinacion(secuencia.get(0)+secuencia.get(1),x,y);
		int turno=0;
		if(secuencia.get(1)==l.getNivel())
			ganado=true;
		for (int j = 0; j < secuencia.get(1); j++) {
			c.addButton(new JButton(""),turno*SEPH+x, y, 15, 15,Color.BLACK,false );	
			c.addColor(Color.BLACK);
			turno++;
		}
		for (int i = 0; i < secuencia.get(0); i++) {
			c.addButton(new JButton(""),turno*SEPH+x, y, 15, 15,Color.WHITE,false );
			c.addColor(Color.WHITE);
			turno++;
		}
		
		return c;
		
		
	}
	
	ActionListener prueba= new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			l.anyadeSolucion(generaResultado(l.getPosBut(XINT),l.getTurno()*SEPV+YINT));
			l.anyadeAPantalla(contentPane, l.getTurno()-1, l.getResult(l.getTurno()-1).getComb().size(),1);
			if(l.getTurno()==l.getIntentos())
				perdido=true;
			if(!ganado && !perdido) {
				l.anyadeIntento(generaConjunto(l.getNivel(),XINT,l.getTurno()*SEPV+YINT,true,false));
				l.anyadeAPantalla(contentPane, l.getTurno(), l.getNivel(),0);
				jugar.setBounds(l.getPosBut(XINT),jugar.getY()+SEPV,jugar.getWidth(),jugar.getHeight());
				l.bloqueaBotones();
				
			}else if(ganado) {
				jugar.setVisible(false);
				JOptionPane.showMessageDialog(null, "Enhorabuena has ganado!!");
			}else if(perdido) {
				jugar.setVisible(false);
				JOptionPane.showMessageDialog(null, "Has perdido!!");
			}
			l.bloqueaBotones();
			contentPane.repaint();
			
		}
	};
	
	ActionListener cambioColor= new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton b = (JButton) e.getSource();
			l.cambiaColorBoton(b);
		}
	};
	
	ActionListener nuevoJuego= new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				nivel n = new nivel();
		        n.setVisible(true);
				dispose();
				
			}
		};
		
	ActionListener close= new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			};
}
