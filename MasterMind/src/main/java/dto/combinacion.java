package dto;

import java.awt.Color;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

public class combinacion {
	
	private final Color COLORES[] = {Color.black, Color.blue, Color.gray, Color.green, Color.magenta, Color.red, Color.orange, Color.yellow};
	private int numeroCasillas; // Cantidad de casillas dentro 
	private ArrayList<JButton> comb;
	private ArrayList<Color> colores;
	private JPanel principal;
	private int id;
	private int x;
	private int y;
	private static int codigo=0;
	private static int name=0;
	
	/**
	 * Constructor
	 * @param numeroCasillas: numero de casillas que contiene la combinacion
	 * @param x: posicion horizontal del primer boton de la combinacion
	 * @param y: posicion vertical del primer boton de la combinacion
	 */
	public combinacion(int numeroCasillas,int x,int y) {
		this.numeroCasillas = numeroCasillas;
		this.comb = new ArrayList<JButton>();
		this.colores = new ArrayList<Color>();
		this.id=codigo;
		this.x =x;
		this.y=y;
		codigo++;
	}
	
	/**
	 * A�ade un boton a la combinacion con color null
	 * @param b: boton a a�adir
	 */
	public void addButton(JButton b,int x,int y,int width,int height,Color color,boolean enabled) {
		b.setBounds(x, y, width, height);
		b.setBackground(color);
		b.setName(String.valueOf(name));
		b.setEnabled(enabled);
		name++;
		this.comb.add(b);
	}
	
	/**
	 * Genera colores aleatorios de toda la lista de colores
	 * @param nivel: cantidad de colores a escoger
	 */
	public void coloresAleatorio(int nivel) {
		ArrayList<Color> col= new ArrayList<Color>();
		
		for (int i = 0; i < nivel; i++) {
			int num = aleatorio(0,COLORES.length-1);
			if (!comprueba(col, num)) {
				col.add(COLORES[num]);
			}else {
				i--;
			}
		}
		for (int i = 0; i < nivel; i++) {
			colores.add( col.get(i));
			comb.get(i).setBackground(col.get(i));
		}
		
	}
	
	/**
	 * Genera una secuencia aleatoria de colores del array
	 * pasado por parametro
	 * @param nivel: catidad de colores a generar
	 * @param colores: colores a escoger
	 */
	public void coloresAleatorio(int nivel,ArrayList<Color> col) {
		ArrayList<Color> elegidos = new ArrayList<Color>();
		for (int i = 0; i < nivel; i++) {
			int num = aleatorio(0,col.size()-1);
			elegidos.add(col.get(num));
		}
		for (int i = 0; i < nivel; i++) {
			this.colores.add( elegidos.get(i));
			comb.get(i).setBackground(elegidos.get(i));
		}
		
	}
	
	/**
	 * Dados un minimo y un maximo genera un numero
	 * aleatorio entre min y max (ambos incluidos)
	 * @param min: numero mas peque�o del rango
	 * @param max: numero mas grande del rango
	 * @return Numero aleatorio entre min y max
	 */
	public int aleatorio(int min, int max) {
        int num = (int)(Math.random()*(max-min+1)+min);
        
        return num;
    }
	
	
	/**
	 * Dados un arrayList de colores y un indice de la constante
	 * COLORES comprueba que el color no este dentro del arrayList
	 * @param col: ArrayList de colores
	 * @param num: indice que marca el color dentro de la constante COLORES
	 * @return cierto si se encuentra y falso si no;
	 */
	public boolean comprueba(ArrayList<Color> col, int num) {
		
		for (int i = 0; i < col.size(); i++) {
			if (col.get(i).equals(COLORES[num])) {
				return true;
			}
		}
		
		return false;
	}


	//GETTERS AND SETTERS

	/**
	 * @return the comb
	 */
	public ArrayList<JButton> getComb() {
		return comb;
	}
	
	/**
	 * @return the colores
	 */
	public ArrayList<Color> getColores() {
		return colores;
	}
	
	/**
	 * Dado un indice de los botones de la combinacion
	 * devuelve el color del fondo del boton
	 * @param indice: numero de boton de la combinacion
	 * @return El color de fondo del boton indicado
	 */
	public Color getColor(int indice) {
		return comb.get(indice).getBackground();
	}
	
	/**
	 * Dado el indice de uno de los colores y un color
	 * modifica el color indicado por el indice, con el
	 * pasado por parametro
	 * @param i: indice de la lista de colores
	 * @param c: color que quedara indicado
	 */
	public void setColor(int i,Color c) {
		colores.set(i, c);
	}

	/**
	 * A�ade un color a la lista de colores
	 * @param c: color a a�adir
	 */
	public void addColor(Color c) {
		colores.add(c);
	}
	
	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Dado el nombre de un boton devuelve el boton de la combinacion
	 * con el mismo nombre
	 * @param nombre: nombre del boton a buscar
	 * @return el boton o null en caso de no coincidencia
	 */
	public JButton getButton(String nombre) {
		for (int i = 0; i < comb.size(); i++) {
			if(comb.get(i).getName().equals(nombre))
				return comb.get(i);
		}
		return null;
	}
	
	/**
	 * Dado el nombre de un boton devuelve el indice del boton
	 * con el mismo nombre
	 * @param nombre: nombre del boton a buscar
	 * @return el indice del boton o -1 en caso de no coincidencia
	 */
	public int getIndexButton(String nombre) {
		for (int i = 0; i < comb.size(); i++) {
			if(comb.get(i).getName().equals(nombre))
				return i;
		}
		return -1;
	}
	
	/**
	 * Dado un color da el color del siguiente boton
	 * @param c: color a buscar del boton
	 * @return el color siguiente o null en caso de no coincidencia
	 */
	public Color siguienteColor(Color c) {
		for (int i = 0; i < comb.size(); i++) {
			if(comb.get(i).getBackground().equals(c)) {
				if(i== comb.size()-1) {
					return comb.get(0).getBackground();
				}else {
					return comb.get(i+1).getBackground();
				}
			}
		}
		return null;
	}
	
}
