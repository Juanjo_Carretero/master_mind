package dto;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JPanel;

public class logica {

	private ArrayList<combinacion> intento;
	private ArrayList<combinacion> resultado;
	private combinacion colPred;
	private combinacion combSecreta;
	private int turno;
	private int nivel;
	private int intentos;
	
	/**
	 * Constructor
	 * @param nivel: cantidad de fichas de la combinacion
	 */
	public logica(int nivel) {
		this.intento= new ArrayList<combinacion>();
		this.resultado= new ArrayList<combinacion>();
		this.colPred = new combinacion(nivel, 180, 45);
		this.combSecreta= new combinacion(nivel, 190, 255);
		this.nivel=nivel;
		this.turno=0;
		switch(nivel) {
			case 4:
				this.intentos = 10;
				break;
			case 5:
				this.intentos = 8;
				break;
			case 6:
				this.intentos = 6;
				break;
		}
	}
	
	/**
	 * Dada una combinacion con los botones inicializados
	 * se añade a la lista de intentos.
	 * @param c: combinacion a añadir
	 */
	public void anyadeIntento(combinacion c) {
		intento.add(c);	
	}

	/**
	 * Dada una combinacion con los botones inicializados
	 * se añade a la lista de resultados y se incrementa el turno
	 * @param c: combinacion a añadir
	 */
	public void anyadeSolucion(combinacion c) {
			resultado.add(c);
			turno++;
	}
	
	/**
	 * Dada una combinacion con los botones inicializados
	 * se asigna esta a la combinacion secreta y se genera
	 * una combinacion de colores aleatoria, con colores 
	 * dentro de los predeterminados
	 * @param c: combinacion a asignar
	 */
	public void anyadeSecreto(combinacion c) {
		combSecreta = c;
		combSecreta.coloresAleatorio(nivel, colPred.getColores());
	}
	
	/**
	 * Dada una combinacion con los botones inicializados se 
	 * asigna esta a los colores predeterminados y se generan
	 * colores aleatorios
	 * @param c: combinacion a asignar
	 */
	public void anyadeColores(combinacion c) {
		colPred = c;
		colPred.coloresAleatorio(nivel);
	}
	
	/**
	 * Añade una combinacion a la pantalla
	 * @param p: panel donde introducir la combinacion
	 * @param indice: numero de prueba a mostrar (null para combinacion
	 * de colores y combinacion secreta)
	 * @param cantidad: numero de casillas contenidas en la combinacion
	 * @param bool: 0 para mostrar intentos, 1 para mostrar resultado
	 * 2 para mostrar combinacion de colores, 3 para mostrar combinacion 
	 * secreta
	 */
	public void anyadeAPantalla(JPanel p,int indice,int cantidad,int bool) {
		switch(bool) {
			case 0:
				for (int i = 0; i < cantidad; i++) {
					p.add(intento.get(indice).getComb().get(i));
				}break;
			case 1:
				for (int i = 0; i < cantidad; i++) {
					p.add(resultado.get(indice).getComb().get(i));
				}break;
			case 2:
				for (int i = 0; i < cantidad; i++) {
					p.add(colPred.getComb().get(i));
				}break;
			case 3:
				for (int i = 0; i < cantidad; i++) {
					p.add(combSecreta.getComb().get(i));
				}
		}
		
	}

	/**
	 * Devuelve un array de enteros con los codigos
	 * PRIMERA POSICION = CASILLAS BLANCAS
	 * SEGUNDA POSICION = CASILLAS NEGRAS
	 * donde cada posicion del array es un boton del intento
	 * @return array con codigos del resultado
	 */
	public ArrayList<Integer> comparaColores() {
		ArrayList<Integer> aux= new ArrayList<Integer>();
		int blancas=0,negras=0;
		aux.add(blancas);
		aux.add(negras);
		for (int i = 0; i < this.nivel; i++) {
			if(colorEnSecreta(intento.get(this.turno).getColor(i))) {
				if(combSecreta.getColor(i).equals(intento.get(this.turno).getColor(i))) {
					aux.set(1, ++negras);
				}else
					aux.set(0, ++blancas);
			}
		}
		return aux;
	}
	
	/**
	 * Dado un color devuelve si el color esta en la
	 * solucion secreta
	 * @param c: color a analizar
	 * @return cierto si esta y falso si no
	 */
	public boolean colorEnSecreta(Color c) {
		for (int i = 0; i < this.nivel; i++) {
			if(c.equals(combSecreta.getColor(i))) {
				return true;
			}
		}
		return false;
	}
	
	
	//GETTERS AND SETTERS
	
	/**
	 * Dado el indice de un resultado devuelve la combinacion
	 * @param i: indice del resultado
	 * @return la combinacion del resultado indicado por parametro
	 */
	public combinacion getResult(int i) {
		return this.resultado.get(i);
	}
	
	/**
	 * Dado el indice de un intento devuelve la combinacion
	 * @param i: indice del intento
	 * @return la combinacion del intento indicado por parametro
	 */
	public combinacion getTry(int i) {
		return this.intento.get(i);
	}
	
	/**
	 * @return el nivel de la partida
	 */
	public int getNivel() {
		return nivel;
	}
	
	/**
	 * @return el turno actual
	 */
	public int getTurno() {
		return turno;
	}
	
	/**
	 * Pasado por parametro la posicion horizontal
	 * base del boton de probar, devuelve la posicion
	 * horizontal del boton actual
	 * @param i: posicion base horizontal del boton jugar
	 * @return posicion horizontal actual
	 */
	public int getPosBut(int i) {
		return nivel*15+i+10;
	}

	/**
	 * Dado un boton lo cambia de color al siguiente de la lista
	 * @param b: boton dado
	 */
	public void cambiaColorBoton(JButton b) {
		Color c = colPred.siguienteColor(b.getBackground());
		b.setBackground(c);
		colPred.setColor(intento.get(turno).getIndexButton(b.getName()), b.getBackground());
		
		
	}
	
	/**
	 * Devuelve el primer color de los colores predeterminados
	 * @return el primer color de colPred
	 */
	public Color getDefaultColor() {
		return colPred.getColor(0);
	}
	
	/**
	 * Bloquea los botones del turno anterior
	 */
	public void bloqueaBotones() {
		ArrayList<JButton> c= intento.get(turno-1).getComb();
		for (int i = 0; i < c.size(); i++) {
			c.get(i).setEnabled(false);
		}
	}

	/**
	 * @return the intentos
	 */
	public int getIntentos() {
		return intentos;
	}

	
	
	
	
	 
	
}
